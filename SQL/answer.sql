

SELECT 
	acts.*,
	bill.*,
	photos.*
FROM accounts acts
	
	JOIN billing bill on acts.id = bill.id
	JOIN photos on acts.id = photos.account_id
	
WHERE
	bill.payment_dt >= DATE_SUB(NOW(), INTERVAL 1 YEAR)
	OR
	(
		acts.approved = 2
		AND 
		acts.created_dt >= DATE_SUB(CURDATE(), INTERVAL 60 DAY)
		AND
		EXISTS (
			SELECT 1 FROM photos 
			WHERE
				photos.approved = 2 
				AND 
				photos.public = 1
				AND
				photos.account_id = acts.id
		)
	)