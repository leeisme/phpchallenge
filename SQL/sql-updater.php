<?php
/**
 * Created by PhpStorm.
 * User: aaronluman
 * Date: 2/19/15
 * Time: 1:17 PM
 */

$mysqli = new mysqli('localhost', 'homestead', 'secret', 'test', 33060);

echo('Yay!'. PHP_EOL);


///** load the existing dump if the databse isn't set up */
//if (end(end($mysqli->query("SELECT COUNT(*) AS count FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMATA.SCHEMA_NAME='test'")->fetch_all(MYSQLI_ASSOC))) == 0)
//{
//    exec('mysql < data.sql');
//}


$mysqli->query('use test');

$sql = "
    UPDATE
      accounts
    SET
      created_dt = DATE_SUB(now(), INTERVAL FLOOR(RAND() * 63072000) SECOND)
";

$mysqli->query($sql);


$sql = "
    UPDATE
      billing AS b
    INNER JOIN
      accounts AS a
    SET
      b.payment_dt = DATE_SUB(now(), INTERVAL FLOOR(RAND() * 63072000) SECOND);
";

$mysqli->query($sql);

$sql = "
    UPDATE
      photos AS p
    INNER JOIN
      accounts AS a
    SET
      p.created_dt = DATE_SUB(now(), INTERVAL FLOOR(RAND() * 63072000) SECOND);
";

$mysqli->query($sql);

//exec('mv data.sql data.'.time().'.sql');
//exec('mysqldump --databases test --add-drop-database --add-drop-table --add-locks > data.sql');