MySQL  test:

In a fictional scenario, users of a dating website can communicate with other users via in-site messaging when they meet ** one of ** the following two criteria:

* Have paid for an upgrade at some point within the last year
* Have an approved account, and at least one approved public photo, and account is less than 60 days old

Using the provided tables(attached as data.sql), construct a single query that will test for either of these conditions when provided with an account id (between 1 and 100).
