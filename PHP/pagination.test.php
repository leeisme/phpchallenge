<?php
include('pagination.class.php');
include_once('textrenderer.class.php');
include_once('htmlrenderer.class.php');

$paginations = array(
    1 => array(
        'page'     => 1,
        'per_page' => 10,
        'total'    => 30,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 9,
        'uri_args' => array('hair-color' => 'brown', 'eye_color' => 'green')
    ),
    2 => array(
        'page'     => 1,
        'per_page' => 10,
        'total'    => 50,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 9,
        'uri_args' => array()
    ),
    3 => array(
        'page'     => 2,
        'per_page' => 10,
        'total'    => 30,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 9,
        'uri_args' => array()
    ),
    4 => array(
        'page'     => 7,
        'per_page' => 10,
        'total'    => 30,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 9,
        'uri_args' => array('id' => 44)
    ),
    5 => array(
        'page'     => 1,
        'per_page' => 10,
        'total'    => 500,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 7,
        'uri_args' => array()
    ),
    6 => array(
        'page'     => 15,
        'per_page' => 10,
        'total'    => 500,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 7,
        'uri_args' => array()
    ),
    7 => array(
        'page'     => 49,
        'per_page' => 10,
        'total'    => 500,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 7,
        'uri_args' => array()
    ),
    8 => array(
        'page'     => 30,
        'per_page' => 10,
        'total'    => 575,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 9,
        'uri_args' => array()
    ),
    9 => array(
        'page'     => 15,
        'per_page' => 7,
        'total'    => 500,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 12,
        'uri_args' => array()
    ),
    10 => array(// this one is tricky because the page is greater than expected, make this so the page is the largest page possible
        'page'     => 50,
        'per_page' => 14,
        'total'    => 500,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 5,
        'uri_args' => array()
    ),
    11 => array(
        'page'     => 500,
        'per_page' => 100,
        'total'    => 50000,
        'url'      => '/pagination/pagination.test.php',
        'width'    => 5,
        'uri_args' => array()
    ),
);

?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="pagination.css" media="all" />
    </head>
    <body>
        <?php
            // Set the renderer.
            pagination::instance()->setRenderer(new HtmlRenderer());
//            pagination::instance()->setRenderer(new TextRenderer());

            foreach($paginations as $pagination) {
                pagination::instance()->render($pagination['page'], $pagination['per_page'], $pagination['total'], $pagination['width'], $pagination['url'], $pagination['uri_args']);
            }
        ?>
    </body>
</html>