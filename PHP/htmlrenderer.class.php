<?php

include_once('renderer.interface.php');
include_once('renderer.class.php');

/**
 * Simple class to render as html output.
 * Class Renderer
 */
class HtmlRenderer extends Renderer implements RendererInterface
{

    /** Wrap $content in <ul> with classes for display
     * @param $content string Html/text content to wrap into return <ul> elements.
     * @return string
     */
    private function wrapInUl($content)
    {
        return '<ul class="nav  pagination">' . $content . '</ul>';
    }


    /**
     * Return a valid url formatted with required params, based on page number.
     * @param $pageNo Integer number of page to link to.
     * @param $noPageNo boolean Suppress output of page number between <a></a> tags.
     * @return string
     */
    private function createPageLink($pageNo, $noPageNo)
    {
        $link = '<a href="' . $this->url;

        if ($pageNo == 0) {
            $pageNo = 1;
        }

        $firstParams = '?pg_page=' . $pageNo . '&pg_per_page=' . $this->per_page .
            '&pg_total=' . $this->total . '&pg_width=' . $this->width;

        $link .= $firstParams;

        $params = '';

        // Add extra params as needed.
        if (count($this->uri_args) > 0) {

            foreach ($this->uri_args as $key => $value) {
                $params .= '&' . $key . '=' . $value;
            }
        }

        $link .= $params . '"';

        if (!$noPageNo) {
            $link .= ">$pageNo</a>";
        } else {
            $link .= "></a>";
        }

        return $link;
    }

    /**  Create <li> element with child <a> link for either prev or next navigation.
     * @param $direct string "previous" or "next" to output respective links.
     * @return string
     */
    private function createDirNav($direct)
    {
        $pageNo = null;
        $classToUse = null;
        $prevPage = null;

        if ($direct == 'previous') {

            $classToUse = 'pagination__previous';

            $pageNo = $this->page - 1;


        } else {

            $classToUse = 'pagination__next';

            $pageNo = $this->page + 1;

        }

        $pageLink = $this->createPageLink($pageNo, true, true);

        // Create the <li> start tag
        $li = "<li " . 'class="' . $classToUse . '"> ';

        // return it all
        return $li . $pageLink . '</li>';

    }

    /**
     * Crete a <li> element with <a> link embedded of page links.
     * @param $pageNo integer Number of page to create a <li><a></a></li> struct for page links.
     * @return string
     */
    private function createListPage($pageNo)
    {
        // Create the <li> start tag
        $li = "<li ";

        if ($pageNo == $this->page) {
            $li .= 'class="current"> ';
        } else {
            $li .= '>';
        }

        $pageLink = $this->createPageLink($pageNo, false, false);

        // return it all
        return $li . $pageLink . '</li>';
    }

    /** Output an ellipsis */
    private function createEllipsis()
    {
        return '<li><a>...</a></li>';
    }


    /**
     * Override doRender to output html pagination.
     * @param $page
     * @param $per_page
     * @param $total
     * @param int $width
     * @param $url
     * @param $uri_args
     * @return string
     */
    public function doRender($page, $per_page, $total, $width = 7, $url, $uri_args)
    {
        // Check page index
        $this->checkAndFixCurrentPage();

        // Comment to view only the rendered navigation element.
        echo ('<p>' .

                '<br /><b>General Info</b>: ' .
                '<br />page: ' . $this->page .
                '<br />per_page: ' . $this->per_page .
                '<br />width: ' . $this->width .
                '<br />page_count: ' . $this->page_count .
                '<br >total: ' . $this->total .
                '<br />block: ' . $this->block .
                '<br />this->block_size: ' . $this->block_size .
                '<br />this->block_count: ' . $this->block_count) .
            '</p><br />';

        $output = '';

        $pagePoints = $this->getStartingEndingPages();

        // Left nav
        $output .= $this->createDirNav('previous');

        // First page
        $output .= $this->createListPage(1);

        // Ellipsis
        $output .= $this->createEllipsis();


        // Loop through and print pages
        for ($i = $pagePoints->start; $i <= $pagePoints->end; $i++) {
            $output .= $this->createListPage($i);
        }


        // Ellipsis
        $output .= $this->createEllipsis();

        // Last Page
        $output .= $this->createListPage($this->page_count);

        $output .= $this->createDirNav('next');


        // Wrap in <ul>
        $output = $this->wrapInUl($output);

        return $output;
    }

}



