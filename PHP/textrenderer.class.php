<?php

include_once('renderer.interface.php');
include_once('renderer.class.php');

/**
 * Simple class to render out an information description about a paginaion Renderer class.
 *
 * (This class has served two fold. On one hand, it was easier to get my head around the pagination logic
 * without the distraction of piecing together html fragments at the same time. This class simply
 * outputs short, linear story logic and metrics representing each test call made by
 * pagination.test.php.
 *
 * Secondly, allows me to break in the Renderer abstraction logic by swapping between
 * TextRenderer and HtmlRenderer to see different results of the model
 * (the pagination params) in different rendered views. As stated, TextRenderer could just as easily
 * been JsonRenderer, XmlRenderer, ImperialGalacticFileRenderer...
 *
 * Class Renderer
 */
class TextRenderer extends Renderer implements RendererInterface
{

    public function doRender($page, $per_page, $total, $width = 7, $url, $uri_args)
    {
        // Check page index
        $this->checkAndFixCurrentPage();

        // echo out basic info
        echo ('<p>' .

                '<br /><b>General Info</b>: ' .
                '<br />page: ' . $this->page .
                '<br />per_page: ' . $this->per_page .
                '<br />width: ' . $this->width .
                '<br />effectiveWidth: ' . $this->effectiveWidth .
                '<br />page_count: ' . $this->page_count .
                '<br >total: ' . $this->total .
                '<br />block: ' . $this->block .
                '<br />this->block_size: ' . $this->block_size .
                '<br />this->block_count: ' . $this->block_count) .
            '</p><br />';


        /* --------------------------------------*/
        /* Blocks
        /* --------------------------------------*/

        echo("<h3>Blocks</h3>");

        $listPageStart = null;
        $listPageEnd = null;

        $pagePoints = $this->getStartingEndingPages();
        $listPageEnd = $pagePoints->end;
        $listPageStart = $pagePoints->start;
        $blockStart = $this->getBlockStart($this->block);
        $blockEnd = $this->getBlockEnd($this->block);

        $this->printLn('block start page: ' . $blockStart);
        $this->printLn('block end page: ' . $blockEnd);
        $this->printLn('List start page: ' . $listPageStart);
        $this->printLn('List end page: ' . $listPageEnd);


        // Warning, Will Robinson.
        if ($this->effectiveWidth < $this->width) {
            $this->printLn('<br /><span class="error">effectiveWidth less than width</span>');
        }

        /* --------------------------------------*/
        /* Pages
        /* --------------------------------------*/

        echo("<h3>Iterate Pages</h3>");

        // Prev and First page elements
        $this->printLn('<< Prev Page');
        $this->printLn('First page: 1');

        echo('<br />');

        // Print out pages
        for ($i = $listPageStart; $i <= $listPageEnd; $i++) {
            echo("---> Page $i");
            if ($this->page == $i) {
                $this->printLn(" CURRENT");
            } else {
                echo('<br />');
            }

        }

        echo ('<br />');

        // Last and Next page elements.
        $this->printLn('Last page: '. ($this->page_count));
        $this->printLn('Next page >>');

        echo('<hr>');
    }

}



