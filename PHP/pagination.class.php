<?php

define('PG_DIR_LEFT', 0);
define('PG_DIR_RIGHT', 1);

// --prefer namespaces/psr auto loading with composer.
// Also would prefer render class to be setup through IoC/DI or through factory.
include_once('renderer.interface.php');
include_once('htmlrenderer.class.php');


class pagination implements RendererInterface
{

    /** Implement singleton access */
    public static function instance()
    {
        /** pagination */
        static $instance = null;

        if ($instance == null) {
            $instance = new pagination();

            // Provide default Renderer implementation.

            // (I don't want any knowledge of implementation around here! It's smelly
            // and tracks rigidity all over the carpet.
            // By the way, did you see the Johnson's down the street? Yup. Brand spanking
            // new ServiceProvider implementation put in over the weekend. Three months before
            // that, they got Dependency Injection installed as well.  Must be nice... )
            $instance->setRenderer(new HtmlRenderer());
        }

        return $instance;
    }

    /** @var  RendererInterface $renderer */
    private $renderer;

    /** Allow setting the renderer to use. */
    public function setRenderer(RendererInterface $rendererInterface)
    {
        $this->renderer = $rendererInterface;
    }

    // Required method/interface/signature
    public function render($page, $per_page, $total, $width = 7, $url, $uri_args)
    {
        return $this->renderer->render($page, $per_page, $total, $width, $url, $uri_args);
    }
}

?>