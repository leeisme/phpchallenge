<?php
/*
 * The project consists of the following types:
 *
 * pagination
 * Class. pagination class is responsible for rendering a <ul> of items representing a paginated
 * list of navigable page links. pagination class implements static singleton access function.
 * pagination class also implements a setRenderer(RendererInterface) function to set a renderer of
 * choice.
 *
 * RendererInterface
 * RendererInterface defines an Interface with one function, render(), using the same
 * signature expected by pagination.test.php.
 *
 * Renderer
 * Abstract class. Renderer implements common methods used to work with a "virtual" pagination
 * structure as defined in the requirements.  Renderer defines a protected abstract function
 * called doRender() with the same signature as the render() function. Descendant classes
 * override doRender() and provide custom output representing a pagination structure. Examples
 * are HTML, XML, JSON, YAML, SandScript, etc.
 *
 * TextRenderer
 * Inherited from Renderer, the TextRenderer is an information dump that I used in order to better
 * understand the paging logic involved a little better. It was faster to understand the
 * logic without the clutter of piecing together html fragments at the same time.
 *
 * HtmlRenderer
 * Inherited from Renderer.  After implementing TextRenderer, I wrote the HtmlRenderer.  The HtmlRenderer
 * outputs the html elements required to display the results of running pagination.test.php.
 *
 * Some parts used slightly "impure" code for convenience. Comments point this
 * out where applicable such as in pagination class. Dependency Injection or abstract factories could
 * help separate out the pagination class from renderer(s) better. Singleton muddies the water a little bit.
 *
 * The pagination.test.php was run with simple output to .html file.
 * vagrant@homestead:~/projects/personal/infogroup_test/PHP$ php pagination.test.php > pagination.class.html
 *
 * Changes made:
 *
 * pagination.class.php
 *  - Add singleton access.
 *  - Add function setRenderer(RendererInterface)
 * pagination.css
 *  - Add css class .error for highlighting output.
 * pagination.test.php
 *  - Change code to swap out default renderer.
 * renderer.interface.php
 *  - Add file.
 * renderer.class.php
 *  - Add file.
 * textrenderer.class.php
 *  - Add file.
 * htmlrenderer.class.php
 *  - Add file.
 *
 *
 *
 */
