<?php

include_once('renderer.interface.php');

/**
 * Simple abstract class to render out a pagination representation.
 *
 * Calculates basic info needed to work with the "structure" of a pagination
 * and then delegates to the doRender method implemented by descendants classes
 * to produce output (html, yaml, json, xml, etc).
 *
 * Central to the design is the concept of a block. A block is analogous to the $width,
 * but used in the context of determining to which segment of available pages, does a particular
 * page belong.
 *
 * If the $width == 9 and there are also 48 pages available, that means 5.33 (6 after rounding up)
 * "blocks" that can be shown at any given time.
 *
 * I also hoped it would provide a consistent snapshot of the pagination view from a user's POV
 * as they navigated page-by-page through the paginator.
 *
 * For instance, clicking next should in most cases render the same output again, but with small
 * differences such as assigning css "current" to the appropriate <li>. Only when
 * it is necessary to progress to a new "block" will the pagination view change in terms of
 * the actual page links shown.  IE: Since pages 2-5 all reside in the first "block" in most cases,
 * the actual page link numbers displayed will stay the same, only the "current" page link will be
 * differentiated as the user navigates from page to page.
 *
 * Essentially, this puts the focus of the paginator logic on rendering a "block" of
 * page links which need to be rendered. Getting the css class assigned to the correct
 * <li> tag, or any further decoration afterwards is simple enough.
 *
 * Descendant classes simply override doRender and use methods of the base class to render
 * output in various formats.
 *
 * See text file, "cursor.diagram.txt" for more information/overview of concept.
 *
 * Class Renderer
 */
abstract class Renderer implements RendererInterface
{

    protected $page;
    protected $block;
    protected $block_size;
    protected $page_count;
    protected $block_count;
    protected $per_page;
    protected $total;
    protected $width;
    protected $effectiveWidth;
    protected $url;
    protected $uri_args;

    protected function printLn($text)
    {
        echo($text . '<br />');
    }

    /**
     * Simple helper method to validate the current page value is within the bounds
     * of the page count.
     */
    protected function checkAndFixCurrentPage()
    {

        if ($this->page < 1) {
            $this->$page = 1;
        } else if ($this->page > $this->page_count) {
            $this->page = $this->page_count;
        }
    }

    /** Calculate basic information about the pagination structure */
    protected function calculateMetrics()
    {
        // Number pages
        $this->page_count = ceil($this->total / $this->per_page);

        $this->block_size = $this->width;

        // Block count.
        $this->block_count = ceil($this->page_count / $this->block_size);

        // Set the current "block" based on page.
        $this->block = $this->getBlockForPage($this->page);

        // We also want to calculate the effective width. In case page_count < block_size.
        if ($this->page_count < $this->block_size) {

            if ($this->page_count == 3) {
                $this->effectiveWidth = 1;
            } else {
                $this->effectiveWidth = $this->page_count;
            }

        } else {
            $this->effectiveWidth = $this->width;
        }
    }

    /** Get the starting page number of a block. */
    protected function getBlockStart($blockNo)
    {
        $start = $this->block_size * $blockNo - $this->block_size + 2;
        return $start;
    }

    protected function getBlockEnd($blockNo)
    {
        return $this->getBlockStart($this->block) + $this->block_size;
    }

    /** Return the "block" number to which a page belongs. If a block is 10 pages wide,
     * then page 5 is always going to fall into the first block, page 15 in block 2...
     */
    protected function getBlockForPage($pageNo)
    {
        // If trying to go past bounds of list then go to last record instead
        if ($pageNo > $this->page_count) {
            $pageNo = $this->page_count;
        }

        if ($pageNo <= $this->block_size) {
            return 1;
        } else if ($pageNo == $this->page_count) {
            return $this->block_count;
        }

        // Loop through and return first block number where $pageNo resides.
        for ($i = 1; $i <= $this->block_count; $i++) {

            $start = $start = $this->block_size * $i - $this->block_size + 1;

            $end = $start + $this->block_size;

            if ($pageNo >= $start && $pageNo <= $end) {
                return $i;
            }
        }

        return -1;
    }

    /** Return a StdClass object with "start" and "end" properties which indicate the
     * safe ranges of pages to print out to the page list. */
    protected function getStartingEndingPages()
    {
        $blockStart = $this->getBlockStart($this->block);
        $blockEnd = $this->getBlockEnd($this->block);


        // If we can't print the full breadth of our $width...
        if ($this->effectiveWidth < $this->width) {

            $listPageStart = $blockStart;

            /* Need to recalculate what the end page will be */

            if ($this->effectiveWidth <= $this->page_count) {

                if ($this->effectiveWidth == 1) {

                    $listPageEnd = $blockStart;

                    $listPageStart = $blockStart;
                } else if ($this->effectiveWidth <= 2) {

                    $listPageEnd = 0;
                    $listPageStart = 0;
                } else {

                    $listPageEnd = $this->effectiveWidth -1;
                }
            } else {
                $listPageEnd = $listPageStart + $this->effectiveWidth - 1;
            }
        } else {

            // The number of available pages are enough to render the entire width

            if ($this->page_count == $this->page) {
                // If the current page == Last Page, calculate range of pages starting
                // with Last Page and offset and backwards.
                $listPageStart = $this->page_count - $this->effectiveWidth;
                $listPageEnd = $this->page_count -1;
            } else if ($this->block == ($this->block_count -1)) {
                // Do not display last page in block, it is the same as Last Page
                $listPageEnd = $blockEnd -2;
                $listPageStart = $blockStart -1;
            } else {
                $listPageStart = $blockStart;
                $listPageEnd = $blockEnd -1;
            }
        }

        $result = new stdClass();
        $result->start = $listPageStart;
        $result->end = $listPageEnd;

        return $result;
    }

    /**
     * Abstract method for generating the output to Consumer.
     * @param $page
     * @param $per_page
     * @param $total
     * @param $width
     * @param $url
     * @param $uri_args
     * @return array
     */
    protected abstract function doRender($page, $per_page, $total, $width, $url, $uri_args);


    /**
     * Implement RendererInterface
     * @param $page
     * @param $per_page
     * @param $total
     * @param int $width
     * @param $url
     * @param $uri_args
     * @return array
     */
    public function render($page, $per_page, $total, $width = 7, $url, $uri_args)
    {

        // Scaffold up the basic info needed for descendant classes

        $this->page = $page;
        $this->per_page = $per_page;
        $this->total = $total;
        $this->width = $width;
        $this->url = $url;
        $this->uri_args = $uri_args;

        // Calculate structure
        $this->calculateMetrics();

        // delegate to abstract method.
        echo($this->doRender($page, $per_page, $total, $width, $url, $uri_args));
    }

}


