<?php

/**
 * Interface for rendering the output.
 * Interface PageRendererInterface
 */
interface RendererInterface
{
    /** Main rendering function. */
    public function render($page, $per_page, $total, $width = 7, $url, $uri_args);

}


