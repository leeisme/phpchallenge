<?php

/*
 * ThreeFive Test
 *
 * Iterate 1 to 100. Print out THREE, FIVE or THREEFIX to denote multiple.
 *
 * Note: Never messed with the bitwise functions much in PHP. Good oppty to
 * dig around. Tried it couple of ways.
 *
 */



echo "Three Five Test". PHP_EOL;


//define('three_bit', 1);
//define('five_bit', 2);
//define('three_five_bit', three_bit|five_bit);

for ($ctr = 1; $ctr < 101; $ctr++) {

    // If the values were interrogated a lot, maybe worth bitwise
//    $flags = 0;

//    if ($ctr % 3 == 0) {
//        $flags |= three_bit;
//        if ($ctr % 5 == 0) {
//            $flags |= five_bit;
//        }
//    } else if ($ctr % 5 == 0) {
//        $flags |= five_bit;
//    }
//
//    if (($flags & three_bit) && ($flags & five_bit)) {
//        echo("THREEFIVE" . PHP_EOL);
//    } else if ($flags & three_bit) {
//        echo("THREE" . PHP_EOL);
//    } else if ($flags & five_bit) {
//        echo("FIVE" . PHP_EOL);
//    } else {
//        echo($ctr . PHP_EOL);
//    }

    if ($ctr % 3 == 0) {
        if ($ctr % 5 == 0) {
            echo ("THREEFIVE". PHP_EOL);
        } else {
            echo ("THREE". PHP_EOL);
        }
    } else if ($ctr % 5 == 0) {
        echo ("FIVE". PHP_EOL);
    } else {
        echo ($ctr. PHP_EOL);
    }
}

?>